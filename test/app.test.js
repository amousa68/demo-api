const app = require("../app")
const request = require("supertest")

describe('request server', () => {
  it('should return with a status of 200 for the root path', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .end(done)
  })

  it('should return with a status of 200 and the correct response', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .expect((res) => {
        expect(res.body.message).toBe('Hello World')
      })
      .end(done)
  })

  it('should return with a status of 404 for an invalid path', (done) => {
    request(app)
      .get('/ddd')
      .expect(404)
      .end(done)
  })
})