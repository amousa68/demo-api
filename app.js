const express = require('express')
const app = express()

app.use(express.json())

app.get('/', (req, res) => {
  return res.send({ message: 'Hello World' })
})

app.all('*', (req, res) => {
  return res.status(404).send({ message: 'Not Found' })
})

module.exports = app